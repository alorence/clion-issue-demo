cmake_minimum_required(VERSION 3.9)
project(clion_cmake_issue_demo)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules)
set(CMAKE_CXX_STANDARD 11)
include(pch)

if(NOT CMAKE_BUILD_TYPE OR CMAKE_BUILD_TYPE STREQUAL "")
    set(CMAKE_BUILD_TYPE Release CACHE STRING "Set build type to Release if not set" FORCE)
endif()


add_subdirectory(Project1)
add_subdirectory(Project2)
add_subdirectory(Project3)
add_subdirectory(Project4)
