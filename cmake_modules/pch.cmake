
include(CMakeParseArguments)

function(export_all_flags _filename lang)

    set(_include_directories "$<TARGET_PROPERTY:${_target},INCLUDE_DIRECTORIES>")
    set(_compile_definitions "$<TARGET_PROPERTY:${_target},COMPILE_DEFINITIONS>")
    set(_compile_flags "$<TARGET_PROPERTY:${_target},COMPILE_FLAGS>")
    set(_compile_options "$<TARGET_PROPERTY:${_target},COMPILE_OPTIONS>")

    set(_include_directories "$<$<BOOL:${_include_directories}>:-I$<JOIN:${_include_directories},\n-I>\n>")
    set(_compile_definitions "$<$<BOOL:${_compile_definitions}>:-D$<JOIN:${_compile_definitions},\n-D>\n>")
    set(_compile_flags "$<$<BOOL:${_compile_flags}>:$<JOIN:${_compile_flags},\n>\n>")
    set(_compile_options "$<$<BOOL:${_compile_options}>:$<JOIN:${_compile_options},\n>\n>")

    if(CMAKE_OSX_DEPLOYMENT_TARGET)
        string(APPEND _additional_flags "${CMAKE_${lang}_OSX_DEPLOYMENT_TARGET_FLAG}${CMAKE_OSX_DEPLOYMENT_TARGET}\n")
    endif()
    if(CMAKE_CXX_STANDARD)
        string(APPEND _additional_flags "${CMAKE_${lang}${CMAKE_${lang}_STANDARD}_EXTENSION_COMPILE_OPTION}\n")
    endif()
    if(CMAKE_VISIBILITY_INLINES_HIDDEN)
        string(APPEND _additional_flags "${CMAKE_CXX_COMPILE_OPTIONS_VISIBILITY_INLINES_HIDDEN}\n")
    endif()
    if(CMAKE_CXX_VISIBILITY_PRESET)
        string(APPEND _additional_flags "${CMAKE_${lang}_COMPILE_OPTIONS_VISIBILITY}${CMAKE_CXX_VISIBILITY_PRESET}\n")
    endif()
    if(CMAKE_OSX_ARCHITECTURES)
        string(APPEND _additional_flags "-arch ${CMAKE_OSX_ARCHITECTURES}\n")
    endif()

    string(TOUPPER ${CMAKE_BUILD_TYPE} _type)
    string(APPEND _additional_flags "${CMAKE_${lang}_FLAGS_${_type}}\n")

    file(GENERATE OUTPUT "${_filename}" CONTENT "${_compile_definitions}${_include_directories}${_compile_flags}${_compile_options}${_additional_flags}\n")
endfunction()



function(add_precompiled_header _target _input)
    cmake_parse_arguments(_PCH "FORCEINCLUDE" "" "" ${ARGN})

    # We will work with absolute path to our PCH header
    get_filename_component(_pch_header_abs ${_input} ABSOLUTE)

    if(NOT EXISTS ${_pch_header_abs})
        message(FATAL_ERROR "The PCH file ${_input} does not exists !")
    endif()

    get_filename_component(_pch_input_filename ${_pch_header_abs} NAME)

    # Retrieve a list of all source files for given target
    get_property(_pch_tgt_files TARGET ${_target} PROPERTY SOURCES)

    if(CMAKE_CXX_COMPILER_ID MATCHES "AppleClang")

        # The directory where Clang will generate our PCH file
        set(_pch_binary_dir "${CMAKE_CURRENT_BINARY_DIR}/${_target}_pch")
        file(MAKE_DIRECTORY "${_pch_binary_dir}")

        # We need to extract compile flags used for the current target to compile our PCH file
        set(_pch_compile_flags_file "${CMAKE_CURRENT_BINARY_DIR}/${_target}_compile_flags.rsp")
        export_all_flags("${_pch_compile_flags_file}" CXX)

        # Fulle path to the PCH file we will generate from our PCH header
        set(_pch_name "${_pch_binary_dir}/cxx_${_pch_input_filename}")
        set(_pch_file "${_pch_name}.pch")

        # Here is the custome command that generate PCH
        # It ouputs the pch file, so the command is automatically called before any compilation of a file set as dependency of this file.
        # See line 87 where the dependency between any source file and the PCH is configured
        add_custom_command(
            OUTPUT "${_pch_file}"
            COMMAND "${CMAKE_CXX_COMPILER}" -x c++-header "${_pch_header_abs}" "@${_pch_compile_flags_file}" -o "${_pch_file}"
            MAIN_DEPENDENCY "${_pch_header_abs}"
            COMMENT "Precompiling ${_pch_input_filename} for ${_target} (C++): ${_pch_file}"
        )

        set_source_files_properties(${_pch_file} PROPERTIES GENERATED TRUE)

        # For all target's source file, append compile flag "-include-pch <pchfile>" option
        foreach(_pch_tgt_file ${_pch_tgt_files})
            if(_pch_tgt_file MATCHES "\\.(cc|cxx|cpp)$")
                set_property(SOURCE ${_pch_tgt_file} APPEND_STRING PROPERTY COMPILE_FLAGS " -include \"${_pch_name}\"")
                # Ensure this source file depends on PCH file, so the custom command to create the PCH is called
                # before compiling this source file, if PCH has ne been generated yet
                set_property(SOURCE ${_pch_tgt_file} APPEND PROPERTY OBJECT_DEPENDS "${_pch_file}")
            endif()
        endforeach()
    else()
        set(msg "This example project is supposed to be compiled using AppleClang on MacOS. The issue "
                "can be reproduced with other compilers, but for simplicity, this function works only with Clang.")
        message(FATAL_ERROR "${msg}")
    endif()
endfunction()
